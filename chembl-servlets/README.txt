
This file contains instructions for installing ChEMBL servlets to
support the Fragment Activity Profiler tool.  For a high level
description of the tool, please consult here:

		     http://tripod.nih.gov/?p=206


Requirements and/or assumptions:

+ Oracle database with sufficient space
+ A recent version of the ChEMBL database installed
+ A recent version (or compatible version) of the Apache Tomcat server


Installation: 

1. Preparing the schema.  Invoke the create.sql script on the ChEMBL
schema; e.g.,

  sqlplus chembl/chembl@//DB @create

Please ask your local DB guru if you don't know how to invoke the SQL
script.  This step will setup the required tables (FRAGMENT_CLASS,
FRAGMENT_INSTANCES, and ACTIVITIES_ROBUSTZ).  If there is any error
during this or any of the subsequent steps,  please invoke the
clean.sql script to clean up.

2. Edit the file WEB-INF/web.xml with the appropriate JDBC URL string;
i.e., replace the string 

   jdbc:oracle:thin:USERNAME/PASSWORD@HOST:1521:SID

with the appropriate values for USERNAME, PASSWORD, HOST, and SID of
your ChEMBLE setup.  This should be all that you need to edit.  

3. Now copy the content of the WEB-INF directory to your Tomcat
webapps directory; e.g.,

   mkdir $TOMCAT/webapps/chembl
   cp -r WEB-INF $TOMCAT/webapps/chembl/

where $TOMCAT is the top-level directory of your Tomcat installation.

4. If all goes well (check the file $TOMCAT/logs/catalina.out), the
servlets are now ready to go.  For first-time installation, the
fragments need to be generated.  To kick off this task, bring
up a browser and type in the URL of your Tomcat server; e.g.,

   http://localhost:8080/chembl/generate-fragment/

which assumes that the installation path in step 3 is used.  Make sure
to include the trailing slash.   This step can take a day or so
depending on your hardware and network connection.  Occasionally check
the file catalina.out for any errors (you can safely ignore WARNING
about max tautomers reached).

If you encounter problems at any time during the steps above, you can
safely invoke the clean.sql script and start over.  Please send
questions/concerns/comments to nguyenda@mail.nih.gov.

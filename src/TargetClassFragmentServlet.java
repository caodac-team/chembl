
import java.io.*;
import java.util.*;
import java.util.zip.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

import chemaxon.formats.*;
import chemaxon.struc.*;
import chemaxon.util.MolHandler;

import org.apache.tomcat.dbcp.dbcp.BasicDataSource;

public class TargetClassFragmentServlet extends HttpServlet {
    static final Logger logger = 
	Logger.getLogger(TargetClassFragmentServlet.class.getName());

    private BasicDataSource ds;

    public void init (ServletConfig config) throws ServletException {
	ServletContext context = config.getServletContext();

	String dbUrl = context.getInitParameter("chembl-url");
	if (dbUrl == null) {
	    throw new ServletException ("No chembl-url parameter defined");
	}
	ds = new BasicDataSource ();
	ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
	ds.setUrl(dbUrl);
    }

    synchronized Connection getConnection () throws SQLException {
	return ds.getConnection();
    }

    protected void doGet (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
	String p = req.getPathInfo().substring(1); // remove leading /
	String[] path = p.length() > 0 ? p.split("/") : null;

	boolean compressed = Boolean.parseBoolean
	    (req.getParameter("compressed"));
	String format = req.getParameter("format");
	if (format == null) {
	    format = "sdf";
	}
	else if (format.equalsIgnoreCase("sdf") 
		 || format.equalsIgnoreCase("json")) {
	}
	else {
	    logger.log(Level.SEVERE, "Unknown format: "+format);
	    return;
	}

	String filename = p.replaceAll("/", "_") + "."+format;
	logger.info(req.getRemoteAddr() +"/" + req.getHeader("X-Real-IP")
		    + ": compressed="+compressed + " path="+p
		    +" format="+format);
	int nerrs = 5;
	do {
	    try {
		if (compressed) {
		    res.setContentType("application/x-gzip");
		    res.setHeader("Content-Disposition",
				  "attachment; filename="+filename+".gz");
		    GZIPOutputStream gzip = 
			new GZIPOutputStream (res.getOutputStream());
		    doFragment (format, gzip, path);
		    gzip.finish();
		}
		else {
		    //res.setContentType("text/plain");
		    res.setContentType("application/"+format);
		    res.setHeader("Content-Disposition",
				  "attachment; filename="+filename);
		    doFragment (format, res.getOutputStream(), path);
		}
		nerrs = 0;
	    }
	    catch (SQLException ex) {
		--nerrs;
		logger.log(Level.SEVERE, "Database error; retrying "+nerrs, ex);
	    }
	}
	while (nerrs > 0);
    }

    protected void doPost (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
	throw new ServletException ("POST is not supported");
    }

    void doFragment (String format, OutputStream os, String... tc) 
	throws SQLException, IOException {
	PrintStream ps = new PrintStream (os, true);

	StringBuilder sql = new StringBuilder ();
	String targetClass = "";
	if (tc != null && tc.length > 0) {
	    sql.append
		("SELECT fc.class_id,"
		 +"  fc.smiles,"
		 +"  AVG(acount) AS acount,"
		 +"  AVG(bcount) AS bcount,"
		 +"  AVG(symmetry) AS symmetry,"
		 +"  AVG(complexity) AS complexity,"
		 +"  AVG(instances) AS instances,"
		 +"  AVG(snr) AS snr_overall,"
		 +"  AVG(apt) AS apt,"
		 +"  AVG(acount)/SQRT(SUM(adiff *adiff)/COUNT(distinct molregno)) AS snr,"
		 +"  COUNT(DISTINCT molregno) AS count,"
		 +"  COUNT(DISTINCT molregno)*LN(AVG(acount)/SQRT(SUM(adiff*adiff)/COUNT(distinct molregno))) AS rank\n"
		 +"FROM fragment_class fc,"
		 +"  fragment_instances fi\n"
		 +"WHERE fc.class_id = fi.class_id\n"
		 +" AND exists \n"
		 +"   (SELECT 1\n"
		 +"    FROM activities_robustz a, "
		 +"     assay2target b,"
		 +"     target_class c\n"
		 +"   WHERE a.assay_id = b.assay_id\n"
		 +"   AND a.molregno = fi.molregno\n"
		 +"   AND b.tid = c.tid\n");
	    for (int i = 1; i <= tc.length; ++i) {
		String clazz = tc[i-1];
		if (clazz != null && clazz.length() > 0) {
		    sql.append("AND c.L"+i+" = '"+clazz+"' ");
		    targetClass += "/"+clazz;
		}
	    }
	    
	    sql.append(") HAVING count (distinct molregno) > 4 "
		       +"AND SQRT(SUM(adiff*adiff)/COUNT(distinct molregno)) > 0\n"
		       +"GROUP BY fc.class_id,fc.smiles\n"
		       +"ORDER BY count*rank/instances desc");
	}
	else {
	    sql.append("SELECT a.*, instances *LN(snr) AS rank "
		       +"FROM fragment_class a "
		       +"WHERE instances > 4 "
		       +"order by count*rank/instances desc");
	}
	//logger.info("SQL "+sql);

	Connection con = getConnection ();
	Statement stm = con.createStatement();

	PreparedStatement pstm = con.prepareStatement
	    ("SELECT AVG(standard_zscore) AS zscore,\n"
	     +"  COUNT(1) AS COUNT\n"
	     +"FROM activities_robustz a,\n"
	     +"  fragment_instances b\n"
	     +"WHERE a.molregno = b.molregno\n"
	     +" AND b.class_id = ?\n"
	     +" AND a.standard_type = 'IC50'\n"
	     +" AND a.standard_units = 'nM'\n"
	     +" AND a.relation = '='\n"
	     +"GROUP BY a.activity_id");

	ResultSet rset = stm.executeQuery(sql.toString());
	int rows = 0;
	if (format.equals("sdf")) {
	    MolHandler mh = new MolHandler ();
	    while (rset.next()) {
		String smiles = rset.getString("smiles");
		try {
		    mh.setMolecule(smiles);
		}
		catch (Exception ex) {
		    logger.log(Level.SEVERE, "Bogus fragment "+smiles, ex);
		}
		
		Molecule mol = mh.getMolecule();
		String classId = rset.getString("class_id");
		mol.setName(classId);
		mol.setProperty("CLASS_ID", classId);
		mol.setProperty("SMILES", smiles);
		mol.setProperty("SYMMETRY", rset.getString("symmetry"));
		mol.setProperty("COMPLEXITY", rset.getString("complexity"));
		mol.setProperty("SNR", String.format
				("%1$.3f", rset.getDouble("snr")));
		mol.setProperty("APT", String.format
				("%1$.3f", rset.getDouble("apt")));
		mol.setProperty("TOTAL", rset.getString("instances"));
		mol.setProperty("COUNT", rset.getString("count"));

		try {
		    mol.setProperty("ZSCORES", getZScores (pstm, classId));
		}
		catch (IOException ex) {
		    logger.log(Level.SEVERE, "Can't get zscores", ex);
		}

		ps.print(mol.toFormat("sdf"));
		++rows;
	    }
	}
	else { // json
	    ps.println("{ \"target-class\": \""+targetClass+"\"");
	    ps.println("  \"fragments\": [");

	    while (rset.next()) {
		if (rows++ > 0) {
		    ps.println(",");
		}
		ps.println("  {\"fragment-id\": "
			   +rset.getString("class_id")+",");
		ps.println("   \"smiles\": \""+rset.getString("smiles")+"\",");
		ps.println("   \"atoms\": "+rset.getString("acount")+",");
		ps.println("   \"bonds\": "+rset.getString("bcount")+",");
		ps.println("   \"symmetry\": "+rset.getString("symmetry")+",");
		ps.println("   \"complexity\": "+rset.getString("complexity")+",");
		ps.println("   \"snr\": "+String.format
			   ("%1.3f", rset.getDouble("snr"))+",");
		ps.println("   \"apt\": "+String.format
			   ("%1.3f", rset.getDouble("apt"))+",");
		ps.println("   \"total\": "+rset.getString("instances")+",");
		ps.print("   \"count\": "+rset.getString("count")+"}");
	    }
	    if (rows > 0) {
		ps.println();
	    }
	    ps.println(" ]");
	    ps.println("}");
	}
	rset.close();
	stm.close();
	pstm.close();
	con.close();
	logger.info(rows+" fragment(s) found!");
	ps.flush();
    }

    // base64 encoded
    String getZScores (PreparedStatement pstm, String classId) 
	throws SQLException, IOException {

	pstm.setString(1, classId);
	ResultSet rs = pstm.executeQuery();
	List<Float> zscores = new ArrayList<Float>();
	while (rs.next()) {
	    zscores.add(rs.getFloat("zscore"));
	}
	rs.close();

	ByteArrayOutputStream bos = new ByteArrayOutputStream ();
	byte[] buf = new byte[4];
	bos.write(encode (zscores.size(), buf), 0, buf.length);
	for (Float z : zscores) {
	    int iz = Float.floatToIntBits(z);
	    bos.write(encode (iz, buf), 0, buf.length);
	}
	return Base64.encodeToString(bos.toByteArray(), true);
    }

    static byte[] encode (int x, byte[] b) {
	b[0] = (byte)(x>>24);
	b[1] = (byte)(x>>16);
	b[2] = (byte)(x>>8);
	b[3] = (byte)(x&0xff);
	return b;
    }
}

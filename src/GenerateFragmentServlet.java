
import java.io.*;
import java.util.*;
import java.util.zip.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

import chemaxon.formats.*;
import chemaxon.struc.*;
import chemaxon.util.MolHandler;

import org.apache.tomcat.dbcp.dbcp.*;

import gov.nih.ncgc.descriptor.MolecularFramework;
import gov.nih.ncgc.util.MolStandardizer;
import gov.nih.ncgc.algo.graph.VFLib2;
import gov.nih.ncgc.util.ChemUtil;

public class GenerateFragmentServlet extends HttpServlet {
    static final Logger logger = 
	Logger.getLogger(GenerateFragmentServlet.class.getName());

    static private final Molecule DONE = new Molecule ();

    class FragmentRunner implements Runnable {
	String name;
	MolecularFramework mf = new MolecularFramework ();
	PreparedStatement pstm1, pstm2, pstm3;
	Connection con;
	int count = 0;

	FragmentRunner (String name) throws SQLException {
	    this.name = name;

	    mf.setGenerateAtomMapping(true);
	    mf.setAllowBenzene(false);
	    mf.setNumThreads(1);
	    mf.setKeepStereo(false);
	    //mf.setKeepFusedRings(true); // generate only ring systems
	}

	public void run () {
	    Thread.currentThread().setName(name);
	    try {
                logger.info(name+": waiting for queue...");
		for (Molecule mol; (mol = queue.take()) != DONE; ) {
		    process (mol);
		}

		closeSQL ();
		logger.info("Fragment thread "+name+ " is done!");
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, "Fragment thread "+name, ex);
	    }
	}

	void newSQL () throws SQLException {
	    logger.info(name+": ** creating new SQL objects; count="
			+count+" **");

	    con = getConnection ();
	    pstm1 = con.prepareStatement
		("insert into fragment_class (smiles,acount,bcount,symmetry,"
		 +"complexity,hashkey1,hashkey2,hashkey3) "
		 +"values (?,?,?,?,?,?,?,?)", new String[]{"class_id"});
	    pstm2 = con.prepareStatement
		("select class_id,hashkey3 from fragment_class "
		 +"where smiles = ?");
	    pstm3 = con.prepareStatement
		("insert into fragment_instances (molregno,chembl_id,class_id"
		 +",mol,smiles,hashkey1,hashkey2,hashkey3,adiff) values"
		 +"(?,?,?,?,?,?,?,?,?)");
	}

	void checkSQL () throws SQLException {
	    if (con == null) {
		newSQL ();
	    }
	    else {
		try {
		    if (con.isClosed()) {
			newSQL ();
		    }
		    Statement stm = con.createStatement();
		    stm.close();
		}
		catch (SQLException ex) {
		    newSQL ();
		}
	    }
	}

	void closeSQL () throws SQLException {
	    if (pstm1 != null) {
		pstm1.close();
	    }
	    if (pstm2 != null) {
		pstm2.close();
	    }
	    if (pstm3 != null) {
		pstm3.close();
	    }
	    if (con != null) {
		con.close();
	    }
	}

	void process (Molecule mol) throws Exception {
	    checkSQL ();

	    if (count % 500 == 0) {
		logger.info(name+": processing "+mol.getName()
			    + "; "+count+" fragments processed!");
	    }

	    mf.setMolecule(mol);
	    mf.run();

            /*
            logger.info(Thread.currentThread().getName()
                        +": "+mol.getName() + " "+mol);
            */

	    int i = 1;
	    for (Enumeration<Molecule> en = mf.getFragments(); 
		 en.hasMoreElements(); ++i) {
		Molecule f = en.nextElement();
		f.setProperty("MOLREGNO", mol.getProperty("MOLREGNO"));
		f.setProperty("CHEMBL_ID", mol.getProperty("CHEMBL_ID"));
		String smiles = MolStandardizer.canonicalSMILES(f, false);
		f.setProperty("SMILES", smiles);
		f.setProperty("HASHKEY", f.getName());
		f.setName(mol.getName()+"-"+i);
		try {
		    load (f, mol.getAtomCount() - f.getAtomCount());
		}
		catch (Exception ex) {
		    logger.log(Level.SEVERE, "Failed to load fragment "
			       +smiles+" for "+mol.getName(), ex);
		}
	    }
	}

	void load (Molecule m, int adiff) throws Exception {
	    String smiles = m.getProperty("SMILES");
            long molregno = Long.parseLong(m.getProperty("MOLREGNO"));
            String chembl = m.getProperty("CHEMBL_ID");
            String hashkey = m.getProperty("HASHKEY");
            String[] hk = hashkey.split("-");
            String hk1 = hk[0], hk2 = hk[0]+hk[1], hk3 = hk[0]+hk[1]+hk[2];

            long classId = 0;
            try {
                pstm1.setString(1, smiles);
		pstm1.setInt(2, m.getAtomCount());
		pstm1.setInt(3, m.getBondCount());
		{
		    Molecule f = m.cloneMolecule();
		    f.aromatize();
		    int complexity = ChemUtil.complexity(f);
		    VFLib2 vf = VFLib2.automorphism(f);
		    int[][] hits = vf.findAll();
		    pstm1.setInt(4, hits.length);
		    pstm1.setInt(5, complexity);
		}
                pstm1.setString(6, hk1);
                pstm1.setString(7, hk2);
                pstm1.setString(8, hk3);
                pstm1.executeUpdate();
                ResultSet rset = pstm1.getGeneratedKeys();
                if (rset.next()) {
                    classId = rset.getLong(1);
                }
                rset.close();
            }
            catch (SQLException ex) {
                //ex.printStackTrace();
                pstm2.setString(1, smiles);
                ResultSet rset = pstm2.executeQuery();
                if (rset.next()) {
                    classId = rset.getLong(1);
                    String key = rset.getString(2);
                    if (!key.equals(hk3)) {
                        logger.warning("Hash key mismatch; expecting "+key
                                       +" but got "+hk3);
                    }
                }
                rset.close();
            }
            
            if (classId == 0) {
                logger.log(Level.SEVERE, name+": ** Can't get class id for "
			   +molregno+"; "+smiles);
            }
	    else {
		pstm3.setLong(1, molregno);
		pstm3.setString(2, chembl);
		pstm3.setLong(3, classId);
		pstm3.setString(4, m.toFormat("sdf"));
		pstm3.setString(5, m.toFormat("smiles:q"));
		pstm3.setString(6, hk1);
		pstm3.setString(7, hk2);
		pstm3.setString(8, hk3);
		pstm3.setInt(9, adiff);
		
		int r = pstm3.executeUpdate();
		if (r > 0) {
		    if (++count % 1000 == 0) {
			logger.info(name+": "+String.format("%1$7d", count)
				    +" " + String.format("%1$6d", classId) 
				    +" " + hashkey);
			//con.commit();
		    }
		}
	    }
	}
    }

    class FragmentationTask implements Runnable {
	public void run () {
	    try {
		doFragmentation ();
	    }
	    catch (SQLException ex) {
		logger.log(Level.SEVERE, "Fragmentation task failed!", ex);
	    }
	}
    }

    class FragmentStatsTask implements Runnable {
	public void run () {
	    try {
		doFragmentStats ();
	    }
	    catch (SQLException ex) {
		logger.log(Level.SEVERE, "Fragment stats task failed!", ex);
	    }
	}
    }

    private BasicDataSource ds;
    private BlockingQueue<Molecule> queue;
    private ExecutorService service;
    private int nthreads = 4;
    private Future[] tasks;
    private AtomicBoolean started = new AtomicBoolean ();

    public void init (ServletConfig config) throws ServletException {
	ServletContext context = config.getServletContext();

	String dbUrl = context.getInitParameter("chembl-url");
	if (dbUrl == null) {
	    throw new ServletException ("No chembl-url parameter defined");
	}

	ds = new BasicDataSource ();
	ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
	ds.setUrl(dbUrl);

	String qsize = context.getInitParameter("queue-size");
	if (qsize == null) {
	    logger.info("queue-size parameter is not defined; "
			+"default to 10000");
	    qsize = "10000";
	}
	else {
	    logger.info("queue-size is "+qsize);
	}
	queue = new ArrayBlockingQueue<Molecule>(Integer.parseInt(qsize));

	String arg = context.getInitParameter("fragment-threads");
	if (arg == null) {
	    logger.info("fragment-threads parameter is not defined; "
			+"default to 4");
	    arg = "4";
	}
	else {
	    logger.info("fragment-threads is "+arg);
	}

	nthreads = Integer.parseInt(arg);
	service = Executors.newCachedThreadPool();
	started.set(false);
    }

    @Override
    public void destroy () {
	logger.info("Shutting down fragment service");

	int running = tasks.length;
	for (Future f : tasks) {
	    if (f.isDone()) {
		--running;
	    }
	}
	if (running != 0) {
	    logger.warning(running + " fragmentation job(s) still active!");
	}

	try {
	    service.shutdown();
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "shutdown", ex);
	}
    }

    synchronized Connection getConnection () throws SQLException {
	return ds.getConnection();
    }

    void kickStart () throws Exception {
	tasks = new Future[nthreads];
	for (int n = 0; n < nthreads; ++n) {
	    tasks[n] = service.submit
		(new FragmentRunner ("FragmentThread-"+n));
	}

	service.submit(new FragmentationTask ());
    }

    protected void doGet (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
	if (!started.getAndSet(true)) {
	    try {
		kickStart ();
	    }
	    catch (Exception ex) {
		error (res, ex.getMessage());
		return;
	    }
	}
	res.setContentType("text/plain");
	PrintWriter pw = res.getWriter();

	int running = tasks.length;
	for (Future f : tasks) {
	    if (f.isDone()) {
		--running;
	    }
	}

        pw.println("Current time: "+new java.util.Date());
	if (running > 0) {
	    pw.println("Fragment queue: "+queue.size());
	    pw.println("Queue remaining capacity: "+queue.remainingCapacity());
	    pw.println("Number of threads: "+nthreads
		       +" ("+running+" still active)");
	}
	else {
            String param = req.getParameter("reset");
            if (param != null && (param.equalsIgnoreCase("yes")
                                  || param.equalsIgnoreCase("true"))) {
                started.set(false);
                pw.println("Generate fragment flag reset; refresh to restart!");
            }
	}

	try {
	    Connection con = getConnection ();
	    Statement stm = con.createStatement();
	    ResultSet rset = stm.executeQuery
		("select count(1) from fragment_class");
	    if (rset.next()) {
		pw.println("Fragment classes: "+rset.getString(1));
	    }
	    rset.close();
	    stm.close();

	    stm = con.createStatement();
	    rset = stm.executeQuery("select count(1) from fragment_instances");
	    if (rset.next()) {
		pw.println("Fragment instances: "+rset.getString(1));
	    }
	    rset.close();
	    stm.close();
	    con.close();
	}
	catch (SQLException ex) {
	    error (res, ex.getMessage());
	}
    }

    protected void doPost (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
	throw new ServletException ("POST is not supported");
    }

    static void error (HttpServletResponse res, String mesg) 
	throws IOException {
	logger.log(Level.SEVERE, mesg);
	res.getWriter().println("** ERROR: "+mesg);
	res.setStatus(HttpServletResponse.SC_BAD_REQUEST);	
    }

    void doFragmentation () throws SQLException {
	Connection con = getConnection ();
	Statement stm = con.createStatement();
	stm.setQueryTimeout(0);

	ResultSet rset = stm.executeQuery
	    ("select * from chembl_id_lookup d, compound_structures b\n"
	     +"where not exists (select 1 from fragment_instances c\n"
	     +"where b.molregno = c.molregno)\n"
	     +"and exists (select 1 from activities_robustz a\n"
	     +"where b.molregno = a.molregno\n"
	     +"and a.molregno = b.molregno\n"
	     +"and a.standard_units = 'nM'\n"
	     //+"and a.STANDARD_FLAG = 1\n"
	     +"and a.STANDARD_TYPE = 'IC50'\n"
	     +"and a.relation = '=')\n"
             +"and d.entity_type = 'COMPOUND'\n"
             +"and d.entity_id = b.molregno\n"
             +"and d.status = 'ACTIVE'");

	logger.info("Prepare to generate fragments...");

	MolHandler mh = new MolHandler ();
	int count = 0;
	while (rset.next()) {
	    String molregno = rset.getString("molregno");
	    try {
		mh.setMolecule(rset.getString("molfile"));
		Molecule mol = mh.getMolecule();
		mol.setProperty("MOLREGNO", molregno);
		mol.setProperty("CHEMBL_ID", rset.getString("chembl_id"));
		mol.setProperty("INCHI_KEY", 
                                rset.getString("standard_inchi_key"));
		mol.setName(mol.getProperty("CHEMBL_ID"));

		int[][] sssr = mol.getSSSR();
		if (sssr.length > 0) {
		    // block if queue full...
		    if (++count % 500 == 0) {
			logger.info("queuing "+mol.getName()+ " "
				    +queue.size()+"/"
				    +queue.remainingCapacity());
		    }
		    queue.put(mol);
		}
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, 
			   "Can't process compound "+molregno, ex);
	    }
	}
	rset.close();
	stm.close();
	con.close();

	logger.info(count+" compound(s) queued!");
	for (int i = 0; i < nthreads; ++i) {
	    try {
		queue.put(DONE);
	    }
	    catch (InterruptedException ex) {
		logger.log(Level.SEVERE, "Queue interrupted", ex);
	    }
	}

	// if there are new instances, we need to update snr, apt, and
	//  instances in fragment_class
	if (count > 0) {
	    logger.info("Waiting for fragmentation jobs to finish...");

	    // waiting for all threads to finish
	    for (Future f : tasks) {
		try {
		    f.get();
		}
		catch (Exception ex) {
		    logger.warning("Fragmentation thread interrupted");
		}
	    }
	}

	doFragmentStats ();
    }

    void doFragmentStats () throws SQLException {
	logger.info("Updating aggregated stats for fragment_class");

	Connection con = getConnection ();
	PreparedStatement pstm = con.prepareStatement
	    ("update fragment_class set snr= ?, apt = ?,instances=? "
	     +"where rowid = ?");
	PreparedStatement pstm2 = con.prepareStatement
	    ("select a.molfile,a.molregno,b.rowid "
	     +"from compound_structures a, fragment_instances b where "
	     +"b.class_id = ? and a.molregno = b.molregno");

	Statement stm = con.createStatement();
	ResultSet rset = stm.executeQuery
	    ("select rowid,smiles,class_id "
	     +"from fragment_class where snr is null");
	MolHandler mh = new MolHandler ();

	int count = 0;
	while (rset.next()) {
	    String rowid = rset.getString(1);
	    String smiles = rset.getString(2);
	    long classId = rset.getLong(3);
	    try {
		mh.setMolecule(smiles);
		Molecule mol = mh.getMolecule();
		int acount = mol.getAtomCount();

		pstm2.setLong(1, classId);
		ResultSet rs = pstm2.executeQuery();
		Map<String, int[]> processed = 
		    new HashMap<String, int[]>();

		double SNR = 0.;
		while (rs.next()) {
		    String molregno = rs.getString("molregno");
		    mh.setMolecule(rs.getString("molfile"));
		    mh.aromatize();
		    mol = mh.getMolecule();
		    int adiff = mol.getAtomCount() - acount;
		    if (!processed.containsKey(molregno)) {
			SNR += adiff*adiff;
			int[] fp = mh.generateFingerprintInInts(16, 2, 6);
			processed.put(molregno, fp);
		    }
		}
		rs.close();

		int[][] N = processed.values().toArray(new int[0][]);
		double APT = 0.;
		if (N.length > 1) {
		    for (int i = 0; i < N.length; ++i) {
			int Ni = 0;
			for (int k = 0; k < N[i].length; ++k) {
			    Ni += Integer.bitCount(N[i][k]);
			}
			for (int j = 0; j < N.length; ++j) {
			    if (i != j) {
				int Nj = 0, Nij = 0;
				for (int k = 0; k < N[j].length; ++k) {
				    Nj += Integer.bitCount(N[j][k]);
				    Nij += Integer.bitCount
					(N[i][k] & N[j][k]);
				}
				APT += (double)Nij/(Ni+Nj-Nij);
			    }
			}
		    }
		    APT /= N.length*(N.length - 1);
		    if (SNR > 0.) {
			SNR = acount/Math.sqrt(SNR/N.length);
		    }
		    else {
			SNR = acount;
		    }
		}
		else {
		    SNR = 0.;
		    APT = 0.;
		}

		pstm.setDouble(1, SNR);
		pstm.setDouble(2, APT);
		pstm.setInt(3, N.length);
		pstm.setString(4, rowid);
		/*
		  logger.info(classId + ": SNR="+SNR+" APT="+APT 
		  + " N="+N.length+ " size="+acount);
		*/
		if (pstm.executeUpdate() > 0) {
		    if (++count % 1000 == 0) {
			logger.info(classId + ": SNR="+SNR+" APT="+APT 
				    + " N="+N.length+ " size="+acount);
		    }
		}
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, "Update error", ex);
	    }
	}
	rset.close();
	stm.close();
	pstm.close();
	pstm2.close();
	con.close();
    }
}

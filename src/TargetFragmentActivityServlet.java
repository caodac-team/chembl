
import java.io.*;
import java.util.*;
import java.util.zip.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

import chemaxon.formats.*;
import chemaxon.struc.*;
import chemaxon.util.MolHandler;

import org.apache.tomcat.dbcp.dbcp.BasicDataSource;

public class TargetFragmentActivityServlet extends HttpServlet {
    static final Logger logger = 
	Logger.getLogger(TargetFragmentActivityServlet.class.getName());

    private BasicDataSource ds;

    public void init (ServletConfig config) throws ServletException {
	ServletContext context = config.getServletContext();

	String dbUrl = context.getInitParameter("chembl-url");
	if (dbUrl == null) {
	    throw new ServletException ("No chembl-url parameter defined");
	}
	ds = new BasicDataSource ();
	ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
	ds.setUrl(dbUrl);
    }

    synchronized Connection getConnection () throws SQLException {
	return ds.getConnection();
    }

    protected void doGet (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
	String p = req.getPathInfo().substring(1); // remove leading /
	String[] path = p.length() > 0 ? p.split("/") : null;
	String fragment = req.getParameter("id");
	boolean compressed = Boolean.parseBoolean
	    (req.getParameter("compressed"));
	String format = req.getParameter("format");
	if (format == null) {
	    format = "json";
	}
	else if (format.equalsIgnoreCase("json") 
		 || format.equals("sdf")) {
	}
	else {
	    logger.log(Level.SEVERE, "Unknown format: "+format);
	    return;
	}

	String filename = p.replaceAll("/", "_")+"."+format;
	logger.info(req.getRemoteAddr() +"/" + req.getHeader("X-Real-IP")
		    + ": fragment="+fragment + " compressed="+compressed
		    + " path="+p + " format="+format);
	if (fragment == null && path == null) {
	    logger.warning("No fragment Id specified");
	}

	Connection con = null;
	try {
	    con = getConnection ();
	    if (compressed) {
		res.setContentType("application/x-gzip");
		if (fragment != null && path != null) {
		    res.setHeader("Content-Disposition",
				  "attachment; filename="+filename+".gz");
		    GZIPOutputStream gzip = 
			new GZIPOutputStream (res.getOutputStream());
		    doFragmentActivity 
			(con, gzip, Long.parseLong(fragment), path);
		    gzip.close();
		}
		else if (path != null) {
		    res.setHeader("Content-Disposition",
				  "attachment; filename="+filename+".gz");
		    GZIPOutputStream gzip = 
			new GZIPOutputStream (res.getOutputStream());
		    doActivity (con, gzip, path);
		    gzip.close();
		}
		else { // fragment != null
		    res.setHeader("Content-Disposition",
				  "attachment; filename="+filename+".gz");
		    GZIPOutputStream gzip = 
			new GZIPOutputStream (res.getOutputStream());
		    doFragment (con, gzip, Long.parseLong(fragment));
		    gzip.close();
		}
	    }
	    else {
		res.setContentType("text/plain");
		if (fragment != null && path != null) {
		    res.setHeader("Content-Disposition",
				  "attachment; filename="+filename+".sdf");
		    doFragmentActivity (con, res.getOutputStream(), 
					Long.parseLong(fragment), path);
		}
		else if (path != null) { 
		    res.setHeader("Content-Disposition",
				  "attachment; filename="+filename+".txt");
		    doActivity (con, res.getOutputStream(),  path);
		}
		else { // fragment
		    res.setHeader("Content-Disposition",
				  "attachment; filename="+filename+".txt");
		    doFragment (con, res.getOutputStream(),  
				Long.parseLong(fragment));
		}
	    }
	}
	catch (SQLException ex) {
	    logger.log(Level.SEVERE, "Database error", ex);
	}
	catch (NumberFormatException ex) {
	    logger.log(Level.SEVERE, "Bogus fragment id "+fragment, ex);
	}
	finally {
	    try {
		if (con != null) {
		    con.close();
		}
	    }
	    catch (SQLException ex) {
		logger.log(Level.SEVERE, "Closing db connection", ex);
	    }
	}
    }

    protected void doPost (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
	throw new ServletException ("POST is not supported");
    }

    // get activity values for a specific target class
    void doActivity (Connection con, OutputStream os, String... tc) 
	throws SQLException, IOException {
	PrintStream ps = new PrintStream (os, true);

	StringBuilder sql = new StringBuilder
	    ("SELECT AVG(standard_zscore) AS zscore,\n"
	     +"  COUNT(1) AS COUNT\n"
	     +"FROM activities_robustz a,\n"
	     +"  assay2target b,\n"
	     +"  target_class c\n"
	     +"WHERE a.assay_id = b.assay_id\n"
	     +" AND b.tid = c.tid\n"
	     );

	String path = "";
	for (int i = 1; i <= tc.length; ++i) {
	    String clazz = tc[i-1];
	    if (clazz != null && clazz.length() > 0) {
		sql.append("AND c.L"+i+" = '"+clazz+"'\n");
		path += "/"+clazz;
	    }
	    else {
		break;
	    }
	}
	sql.append("GROUP BY a.activity_id");

	Statement stm = con.createStatement();
	ResultSet rset = stm.executeQuery(sql.toString());
	int rows = 0;

	ps.println("# activities for "+path);
	while (rset.next()) {
	    double z = rset.getDouble("zscore");
	    ps.println(String.format("%1$.5f", z));
	    ++rows;
	}
	rset.close();
	stm.close();
	ps.flush();
	logger.info(rows+" activities for "+path);
    }

    // get activity values for a specific fragment
    void doFragment (Connection con, OutputStream os, long fragId) 
	throws SQLException, IOException {
	PrintStream ps = new PrintStream (os, true);

	StringBuilder sql = new StringBuilder
	    ("SELECT AVG(standard_zscore) AS zscore,\n"
	     +"  COUNT(1) AS COUNT\n"
	     +"FROM activities_robustz a,\n"
	     +"  fragment_instances b\n"
	     +"WHERE a.molregno = b.molregno\n"
	     +" AND b.class_id = "+fragId+"\n"
	     +"GROUP BY a.activity_id");

	Statement stm = con.createStatement();
	ResultSet rset = stm.executeQuery(sql.toString());
	int rows = 0;

	ps.println("# activities for fragment "+fragId);
	while (rset.next()) {
	    double z = rset.getDouble("zscore");
	    ps.println(String.format("%1$.5f", z));
	    ++rows;
	}
	rset.close();
	stm.close();
	ps.flush();
	logger.info(rows+" activities for fragment "+fragId);
    }

    void doFragmentActivity (Connection con, OutputStream os, 
			     long fragId, String... tc) 
	throws SQLException, IOException {
	PrintStream ps = new PrintStream (os, true);

	StringBuilder sql = new StringBuilder 
	    ("SELECT a.*,e.smiles,e.chembl_id,f.molfile,g.doi,g.pubmed_id\n"
	     +"FROM activities_robustz a, \n"
	     +"   assay2target b,\n"
	     +"     target_class c,\n"
	     +"     fragment_class d,\n"
	     +"     fragment_instances e,\n"
	     +"     compound_structures f,\n"
	     +"     docs g\n"
	     +"WHERE a.assay_id = b.assay_id\n"
	     +"   AND b.tid = c.tid\n"
	     +"   AND a.doc_id = g.doc_id\n"
	     +"   AND d.class_id = "+fragId+"\n"
	     +"   AND e.class_id = d.class_id\n"
	     +"   AND e.molregno = a.molregno\n"
	     +"   and e.molregno = f.molregno\n"
	     +"   AND a.standard_type = 'IC50'\n"
	     +"   AND a.standard_units = 'nM'\n"
	     +"   AND a.relation = '='\n");

	for (int i = 1; i <= tc.length; ++i) {
	    String clazz = tc[i-1];
	    if (clazz != null && clazz.length() > 0) {
		sql.append("AND c.L"+i+" = '"+clazz+"'\n");
	    }
	    else {
		break;
	    }
	}
	sql.append("order by a.molregno");
	     
	//logger.info("SQL "+sql);
	Statement stm = con.createStatement();
	ResultSet rset = stm.executeQuery(sql.toString());
	int rows = 0, mols = 0;
	MolHandler mh = new MolHandler ();
	Molecule mol = null;
	Set<String> set = new HashSet<String>();
	Set<String> docs = new HashSet<String>();
	while (rset.next()) {
	    String molregno = rset.getString("molregno");

	    String actid = rset.getString("activity_id");
	    if (set.contains(actid)) {
		continue;
	    }

	    String doc = rset.getString("doc_id");
	    if (mol == null || !mol.getName().equals(molregno)) {
		if (mol != null) {
		    ps.print(mol.toFormat("sdf"));
		}

		try {
		    mh.setMolecule(rset.getString("molfile"));
		    mol = mh.getMolecule();
		    mol.setName(molregno);
		    mol.setProperty("MOLREGNO", mol.getName());
		    mol.setProperty("FRAGMENT", rset.getString("smiles"));
		    mol.setProperty("CHEMBL_ID", rset.getString("chembl_id"));
		    String zscore = String.format
			("%1$.3f", rset.getDouble("standard_zscore"));
		    mol.setProperty("ZSCORE", zscore);
		    updateRef (mol, rset);
		    ++mols;
		}
		catch (Exception ex) {
		    logger.log(Level.SEVERE, "Bogus mofile", ex);
		}
	    }
	    else {
		String zscore = String.format
		    ("%1$.3f", rset.getDouble("standard_zscore"));
		mol.setProperty("ZSCORE", 
				mol.getProperty("ZSCORE")+"\n"+zscore);
		if (!docs.contains(doc)) {
		    updateRef (mol, rset);
		}
	    }
	    set.add(actid);
	    docs.add(doc);

	    ++rows;
	}
	if (mol != null) {
	    ps.print(mol.toFormat("sdf"));
	}

	rset.close();
	stm.close();
	ps.flush();
	logger.info(rows+" activities for "+mols+" compounds!");
    }

    void updateRef (Molecule mol, ResultSet rset) throws SQLException {
	String ref = mol.getProperty("REF");
	String doi = rset.getString("doi");
	String pubmed = rset.getString("pubmed_id");
	if (ref == null) {
	    if (doi != null) ref = doi;
	    else if (pubmed != null) ref = pubmed;
	}
	else if (doi != null) {
	    ref += "\n"+doi;
	}
	else if (pubmed != null) {
	    ref += "\n"+pubmed;
	}
	mol.setProperty("REF", ref);
    }
}

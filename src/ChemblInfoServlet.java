
import java.io.*;
import java.util.*;
import java.util.zip.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

import org.apache.tomcat.dbcp.dbcp.*;

public class ChemblInfoServlet extends HttpServlet {
    static final Logger logger = 
	Logger.getLogger(ChemblInfoServlet.class.getName());

    private String version;

    public void init (ServletConfig config) throws ServletException {
	ServletContext context = config.getServletContext();

	String dbUrl = context.getInitParameter("chembl-url");
	if (dbUrl == null) {
	    throw new ServletException ("No chembl-url parameter defined");
	}

	BasicDataSource ds = new BasicDataSource ();
	ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
	ds.setUrl(dbUrl);

	try {
	    Connection con = ds.getConnection();
	    Statement stm = con.createStatement();
	    ResultSet rset = stm.executeQuery("select * from version");
	    if (rset.next()) {
		version = rset.getString("comments");
	    }
	    rset.close();
	    stm.close();
	    con.close();
	}
	catch (SQLException ex) {
	    logger.log(Level.SEVERE, "Database error", ex);
	}
    }

    protected void doGet (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
	res.setContentType("text/plain");
	res.getWriter().println(version);
    }

    protected void doPost (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
	throw new ServletException ("POST is not supported");
    }
}


import java.io.*;
import java.util.*;
import java.util.zip.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

import chemaxon.formats.*;
import chemaxon.struc.*;
import chemaxon.util.MolHandler;

import org.apache.tomcat.dbcp.dbcp.*;

import gov.nih.ncgc.descriptor.MolecularFramework;
import gov.nih.ncgc.util.MolStandardizer;
import gov.nih.ncgc.util.ChemUtil;

public class StandardizeServlet extends HttpServlet {
    static final Logger logger = 
	Logger.getLogger(StandardizeServlet.class.getName());

    static private final Molecule DONE = new Molecule ();

    class StandardizeRunner implements Runnable {
	String name;
	PreparedStatement pstm;
	Connection con;
	int count = 0;
	MolStandardizer mstd = new MolStandardizer ();

	StandardizeRunner (String name) throws SQLException {
	    this.name = name;
	}

	public void run () {
	    Thread.currentThread().setName(name);
	    try {
		for (Molecule mol; (mol = queue.take()) != DONE; ) {
		    process (mol);
		}

		closeSQL ();
		logger.info("Standardize thread "+name+ " is done!");
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, "Standardize thread "+name, ex);
	    }
	}

	void newSQL () throws SQLException {
	    logger.info(name+": ** creating new SQL objects; count="
			+count+" **");

	    con = getConnection ();
	    pstm = con.prepareStatement
		("update compound_structures "
		 +"set hash1 = ?, hash2 = ?, hash3 = ?, std_smiles=? "
		 +"where rowid = ?");
	}

	void checkSQL () throws SQLException {
	    if (con == null) {
		newSQL ();
	    }
	    else {
		try {
		    if (con.isClosed()) {
			newSQL ();
		    }
		    Statement stm = con.createStatement();
		    stm.close();
		}
		catch (SQLException ex) {
		    newSQL ();
		}
	    }
	}

	void closeSQL () throws SQLException {
	    if (pstm != null) {
		pstm.close();
	    }
	    if (con != null) {
		con.close();
	    }
	}

	void process (Molecule mol) throws Exception {
	    checkSQL ();

	    if (count % 500 == 0) {
		logger.info(name+": processing "+mol.getName()
			    + "; "+count+" structures processed!");
	    }

	    if (!mstd.standardize(mol)) {
		logger.warning("Can't standardize structure "+mol.getName());
	    }

	    String smiles = MolStandardizer.canonicalSMILES(mol);
	    String[] hkeys = MolStandardizer.hashKeyArray(mol);
	    pstm.setString(1, hkeys[0]);
	    pstm.setString(2, hkeys[0]+hkeys[1]);
	    pstm.setString(3, hkeys[0]+hkeys[1]+hkeys[2]);
	    pstm.setString(4, smiles);
	    pstm.setString(5, mol.getProperty("ROWID"));
	    if (pstm.executeUpdate() > 0) {
		++count;
	    }
	}
    }

    class StandardizeTask implements Runnable {
	public void run () {
	    try {
		doStandardize ();
	    }
	    catch (SQLException ex) {
		logger.log(Level.SEVERE, "Standardize task failed!", ex);
	    }
	}
    }

    private BasicDataSource ds;
    private BlockingQueue<Molecule> queue;
    private ExecutorService service;
    private int nthreads = 4;
    private Future[] tasks;
    private AtomicBoolean started = new AtomicBoolean ();

    public void init (ServletConfig config) throws ServletException {
	ServletContext context = config.getServletContext();

	String dbUrl = context.getInitParameter("chembl-url");
	if (dbUrl == null) {
	    throw new ServletException ("No chembl-url parameter defined");
	}

	ds = new BasicDataSource ();
	ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
	ds.setUrl(dbUrl);

	String qsize = context.getInitParameter("queue-size");
	if (qsize == null) {
	    logger.info("queue-size parameter is not defined; "
			+"default to 10000");
	    qsize = "10000";
	}
	else {
	    logger.info("queue-size is "+qsize);
	}
	queue = new ArrayBlockingQueue<Molecule>(Integer.parseInt(qsize));

	String arg = context.getInitParameter("standardize-threads");
	if (arg == null) {
	    logger.info("standardize-threads parameter is not defined; "
			+"default to 4");
	    arg = "4";
	}
	else {
	    logger.info("standardize-threads is "+arg);
	}

	nthreads = Integer.parseInt(arg);
	service = Executors.newCachedThreadPool();
	started.set(false);
    }

    @Override
    public void destroy () {
	logger.info("Shutting down standardize service");

	int running = tasks.length;
	for (Future f : tasks) {
	    if (f.isDone()) {
		--running;
	    }
	}
	if (running != 0) {
	    logger.warning(running + " standardize job(s) still active!");
	}

	try {
	    service.shutdown();
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "shutdown", ex);
	}
    }

    synchronized Connection getConnection () throws SQLException {
	return ds.getConnection();
    }

    void kickStart () throws Exception {
	tasks = new Future[nthreads];
	for (int n = 0; n < nthreads; ++n) {
	    tasks[n] = service.submit
		(new StandardizeRunner ("StandardizeThread-"+n));
	}

	service.submit(new StandardizeTask ());
    }

    protected void doGet (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
	if (!started.getAndSet(true)) {
	    try {
		kickStart ();
	    }
	    catch (Exception ex) {
		error (res, ex.getMessage());
		return;
	    }
	}
	res.setContentType("text/plain");
	PrintWriter pw = res.getWriter();

	int running = tasks.length;
	for (Future f : tasks) {
	    if (f.isDone()) {
		--running;
	    }
	}

	if (running > 0) {
	    pw.println("Standardize queue: "+queue.size());
	    pw.println("Queue remaining capacity: "+queue.remainingCapacity());
	    pw.println("Number of threads: "+nthreads
		       +" ("+running+" still active)");
	}
	else {
	}

	try {
	    Connection con = getConnection ();
	    Statement stm = con.createStatement();
	    ResultSet rset = stm.executeQuery
		("select count(1) from "
		 +"compound_structures where std_smiles is not null");
	    if (rset.next()) {
		pw.println("Structure standardized: "+rset.getString(1));
	    }
	    rset.close();
	    stm.close();
	    con.close();
	}
	catch (SQLException ex) {
	    error (res, ex.getMessage());
	}
    }

    protected void doPost (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
	throw new ServletException ("POST is not supported");
    }

    static void error (HttpServletResponse res, String mesg) 
	throws IOException {
	logger.log(Level.SEVERE, mesg);
	res.getWriter().println("** ERROR: "+mesg);
	res.setStatus(HttpServletResponse.SC_BAD_REQUEST);	
    }

    void doStandardize () throws SQLException {
	Connection con = getConnection ();
	Statement stm = con.createStatement();
	stm.setQueryTimeout(0);

	ResultSet rset = stm.executeQuery
	    ("select molregno,molfile,rowid from compound_structures "
	     +"where std_smiles is null");

	logger.info("Prepare to standardize structures...");

	MolHandler mh = new MolHandler ();
	int count = 0;
	while (rset.next()) {
	    String molregno = rset.getString("molregno");
	    String rowid = rset.getString("rowid");
	    try {
		mh.setMolecule(rset.getString("molfile"));
		Molecule mol = mh.getMolecule();
		mol.setProperty("MOLREGNO", molregno);
		mol.setProperty("ROWID", rowid);
		mol.setName(molregno);

		queue.put(mol);
	    }
	    catch (Exception ex) {
		logger.log(Level.SEVERE, 
			   "Can't process compound "+molregno, ex);
	    }
	}
	rset.close();
	stm.close();
	con.close();

	logger.info(count+" structure(s) queued!");
	for (int i = 0; i < nthreads; ++i) {
	    try {
		queue.put(DONE);
	    }
	    catch (InterruptedException ex) {
		logger.log(Level.SEVERE, "Queue interrupted", ex);
	    }
	}

	// if there are new instances, we need to update snr, apt, and
	//  instances in fragment_class
	if (count > 0) {
	    logger.info("Waiting for standardize jobs to finish...");

	    // waiting for all threads to finish
	    for (Future f : tasks) {
		try {
		    f.get();
		}
		catch (Exception ex) {
		    logger.warning("Standardize thread interrupted");
		}
	    }
	}
    }
}

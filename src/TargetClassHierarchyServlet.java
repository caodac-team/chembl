
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

import javax.swing.tree.DefaultMutableTreeNode;


public class TargetClassHierarchyServlet extends HttpServlet {
    static final Logger logger = 
	Logger.getLogger(TargetClassHierarchyServlet.class.getName());

    static {
	try {
	    Class.forName("oracle.jdbc.driver.OracleDriver");
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, "Can't load oracle driver", ex);
	}
    }

    private DefaultMutableTreeNode treeRoot;
    
    public void init (ServletConfig config) throws ServletException {
	ServletContext context = config.getServletContext();

	String dbUrl = context.getInitParameter("chembl-url");
	if (dbUrl == null) {
	    throw new ServletException ("No chembl-url parameter defined");
	}

	try {
	    logger.info("preloading target class hierarchy...");
	    createTargetClassHierarchy (dbUrl);
	}
	catch (Exception ex) {
	    logger.log(Level.SEVERE, 
		       "Can't construct target class tree model", ex);
	    throw new ServletException (ex);
	}
    }

    void createTargetClassHierarchy (String dbUrl) throws Exception {
	// construct the tree model upon startup... this servlet should
	//   be run on server startup

	Connection con = DriverManager.getConnection(dbUrl);
	Statement stm = con.createStatement();
	ResultSet rset = stm.executeQuery
	    ("select l1,l2,l3,l4,l5,l6,l7,l8,count(1) as cnt "
	     +"from target_class a, assay2target b, activities_robustz c "
	     +"where a.tid = b.tid and b.assay_id = c.assay_id "
	     +"group by l1,l2,l3,l4,l5,l6,l7,l8 "
	     +"order by l1,l2,l3,l4,l5,l6,l7,l8");
	
	Map root = new TreeMap();
	int rows = 0;
	while (rset.next()) {
	    Map n = root;
	    for (int l = 1; l <= 8; ++l) {
		String name = rset.getString("L"+l);
		if (name == null) {
		    break;
		}
		Map child = (Map)n.get(name);
		if (child == null) {
		    child = new TreeMap ();
		    n.put(name, child);
		}
		n = child;
	    }
	    n.put("_size", rset.getInt("cnt"));
	    ++rows;
	}
	logger.info(rows+" row(s) fetched!");

	treeRoot = new DefaultMutableTreeNode ();
	createTree (treeRoot, root);
	adjustSize (treeRoot);
	
	rset.close();
	stm.close();
	con.close();
    }

    void createTree (DefaultMutableTreeNode node, Map map) {
	Integer size = (Integer)map.remove("_size");
	Map value = (Map)node.getUserObject();
	if (value != null) {
	    value.put("size", size);
	}
	for (Object key : map.keySet()) {
	    value = new TreeMap ();
	    value.put("name", key);
	    DefaultMutableTreeNode n = new DefaultMutableTreeNode (value);
	    node.add(n);
	    createTree (n, (Map)map.get(key));
	}
    }

    void adjustSize (DefaultMutableTreeNode node) {
	for (Enumeration en = node.depthFirstEnumeration(); 
	     en.hasMoreElements(); ) {
	    DefaultMutableTreeNode n = (DefaultMutableTreeNode)en.nextElement();
	    DefaultMutableTreeNode p = (DefaultMutableTreeNode)n.getParent();
	    if (p != null) {
		Map value = (Map)p.getUserObject();

		Integer size = 0;
		if (value == null) {
		    p.setUserObject(value = new TreeMap ());
		}
		else {
		    size = (Integer)value.get("size");
		    if (size == null) {
			size = 0;
		    }
		}

		Map map = (Map)n.getUserObject();
		value.put("size", size + (Integer)map.get("size"));
	    }
	}
    }

    protected void doGet (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
	if (treeRoot != null) {
	    res.setContentType("text/xml");
	    toTreeML (res.getWriter());
	}
    }

    protected void doPost (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
	if (treeRoot != null) {
	    res.setContentType("text/xml");
	    toTreeML (res.getWriter());
	}
    }

    void toTreeML (PrintWriter pw) {
	pw.println("<?xml version=\"1.0\"?>");
	pw.println("<tree>");
	pw.println(" <declarations>");
	pw.println("   <attributeDecl name=\"name\" type=\"String\"/>");
	pw.println("   <attributeDecl name=\"size\" type=\"Int\"/>");
	pw.println(" </declarations>");
	toTreeML (pw, treeRoot);
	pw.println("</tree>");
    }

    void toTreeML (PrintWriter pw, DefaultMutableTreeNode node) {
	StringBuilder pad = new StringBuilder ();
	for (int i = 0; i <= node.getLevel(); ++i) {
	    pad.append(" ");
	}
	pw.println(pad+(node.isLeaf() ? "<leaf>":"<branch>"));
	Map map = (Map)node.getUserObject();
	if (map != null) {
	    String name = (String)map.get("name");
	    if (name != null) {
		pw.println(pad+" <attribute name=\"name\" value=\""
			   +name+"\"/>");
	    }
	    Integer size = (Integer)map.get("size");
	    if (size != null) {
		pw.println(pad+" <attribute name=\"size\" value=\""
			   +size+"\"/>");
	    }
	}

	for (Enumeration en = node.children(); en.hasMoreElements(); ) {
	    DefaultMutableTreeNode child = 
		(DefaultMutableTreeNode)en.nextElement();
	    toTreeML (pw, child);
	}
	pw.println(pad+(node.isLeaf() ? "</leaf>":"</branch>"));
    }
}

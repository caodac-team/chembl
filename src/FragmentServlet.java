
import java.io.*;
import java.util.*;
import java.util.zip.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

import chemaxon.formats.*;
import chemaxon.struc.*;
import chemaxon.util.MolHandler;

import org.apache.tomcat.dbcp.dbcp.*;

public class FragmentServlet extends HttpServlet {
    static final Logger logger = 
	Logger.getLogger(FragmentServlet.class.getName());

    private BasicDataSource ds;

    public void init (ServletConfig config) throws ServletException {
	ServletContext context = config.getServletContext();

	String dbUrl = context.getInitParameter("chembl-url");
	if (dbUrl == null) {
	    throw new ServletException ("No chembl-url parameter defined");
	}

	ds = new BasicDataSource ();
	ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
	ds.setUrl(dbUrl);
    }

    synchronized Connection getConnection () throws SQLException {
	//return DriverManager.getConnection(dbUrl);
	return ds.getConnection();
    }

    protected void doGet (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
	boolean compressed = Boolean.parseBoolean
	    (req.getParameter("compressed"));
	String format = req.getParameter("format");
	if (format == null) {
	    format = "sdf";
	}
	else if (format.equalsIgnoreCase("sdf") 
		 || format.equalsIgnoreCase("json")) {
	}
	else {
	    logger.log(Level.SEVERE, "Unknown format: "+format);
	    return;
	}

	String arg, sql = null;
	if ((arg = req.getParameter("id")) != null) {
	    // get fragments by id
	}
	else if ((arg = req.getParameter("hk")) != null) {
	    // get fragments by hash key
	    String[] hashkeys = arg.split(",");
	    int n = Math.min(999, hashkeys.length);
	    if (n == 1) {
		sql = "select * from fragment_class where hashkey3 = '"
		    +hashkeys[0]+"'";
	    }
	    else if (n > 1) {
		StringBuilder sb = new StringBuilder 
		    ("select * from fragment_class where hashkey3 in (");
		sb.append("'"+hashkeys[0]+"'");
		for (int i = 1; i < n; ++i) {
		    sb.append(",'"+hashkeys[i]+"'");
		}
		sb.append(")");
		sql = sb.toString();
	    }
	}
	else {
	    // fetch random fragments 
	    int size = 100;
	    if ((arg = req.getParameter("size")) != null) {
		try {
		    size = Integer.parseInt(arg);
		}
		catch (NumberFormatException ex) {
		    logger.log(Level.SEVERE, "Bogus size value: "+arg);
		}
	    }
	    int acount = 10;
	    if ((arg = req.getParameter("acount")) != null) {
		try {
		    acount = Integer.parseInt(arg);
		}
		catch (NumberFormatException ex) {
		    logger.log(Level.SEVERE, "Bogus acount value: "+arg);
		}
	    }
	    int instances = 20;
	    if ((arg = req.getParameter("instances")) != null) {
		try {
		    instances = Integer.parseInt(arg);
		}
		catch (NumberFormatException ex) {
		    logger.log(Level.SEVERE, "Bogus instances value: "+arg);
		}
	    }
	    double snr = 0.8;
	    if ((arg = req.getParameter("snr")) != null) {
		try {
		    snr = Double.parseDouble(arg);
		}
		catch (NumberFormatException ex) {
		    logger.log(Level.SEVERE, "Bogus snr value: "+arg);
		}
	    }
	    sql = "SELECT *\n"
		+"FROM\n"
		+"  (SELECT a.*,\n"
		+"     dbms_random.VALUE AS rnd\n"
		+"   FROM fragment_class a\n"
		+"   WHERE acount > "+acount+"\n"
		+"   AND instances > "+instances+"\n"
		+"   AND snr > "+snr+"\n"
		+"   ORDER BY rnd)\n"
		+"WHERE rownum <= "+size;
	}

	if (sql != null) {
	    int nerrs = 5;
	    do {
		try {
		    if (compressed) {
			res.setContentType("application/x-gzip");
			GZIPOutputStream gzip = new GZIPOutputStream 
			    (res.getOutputStream());
			doSQL (format, gzip, sql);
			gzip.finish();
		    }
		    else {
			res.setContentType("application/"+format);
			doSQL (format, res.getOutputStream(), sql);
		    }
		    nerrs = 0;
		}
		catch (SQLException ex) {
		    --nerrs;
		    logger.log(Level.SEVERE, "SQL exception", ex);
		}
	    }
	    while (nerrs > 0);
	}
    }

    protected void doPost (HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
	throw new ServletException ("POST is not supported");
    }

    void doSQL (String format, OutputStream os, String sql) 
	throws SQLException {
	PrintStream ps = new PrintStream (os, true);

	Connection con = getConnection ();
	Statement stm = con.createStatement();
	PreparedStatement pstm = con.prepareStatement
	    ("SELECT AVG(standard_zscore) AS zscore,\n"
	     +"  COUNT(1) AS COUNT\n"
	     +"FROM activities_robustz a,\n"
	     +"  fragment_instances b\n"
	     +"WHERE a.molregno = b.molregno\n"
	     +" AND b.class_id = ?\n"
	     +" AND a.standard_type = 'IC50'\n"
	     +" AND a.standard_units = 'nM'\n"
	     +" AND a.relation = '='\n"
	     +"GROUP BY a.activity_id");

	ResultSet rset = stm.executeQuery(sql);

	long start = System.currentTimeMillis();
	int rows = 0;
	if (format.equalsIgnoreCase("sdf")) {
	    MolHandler mh = new MolHandler ();
	    while (rset.next()) {
		String smiles = rset.getString("smiles");
		try {
		    mh.setMolecule(smiles);
		}
		catch (Exception ex) {
		    logger.log
			(Level.SEVERE, "Can't parse smiles: "+smiles, ex);
		    continue;
		}
		Molecule mol = mh.getMolecule();

		String classId = rset.getString("class_id");
		mol.setName(classId);
		mol.setProperty("CLASS_ID", mol.getName());
		mol.setProperty("SMILES", smiles);
		mol.setProperty("SYMMETRY", rset.getString("symmetry"));
		mol.setProperty("COMPLEXITY", rset.getString("complexity"));
		mol.setProperty("SNR", String.format
				("%1$.3f", rset.getDouble("snr")));
		mol.setProperty("APT", String.format
				("%1$.3f", rset.getDouble("apt")));
		mol.setProperty("COUNT", rset.getString("instances"));
		mol.setProperty("HASHKEY", rset.getString("hashkey3"));
		try {
		    mol.setProperty("ZSCORES", getZScores (pstm, classId));
		}
		catch (IOException ex) {
		    logger.log(Level.SEVERE, "Can't get zscores", ex);
		}

		ps.print(mol.toFormat("sdf"));
		++rows;
	    }
	}
	else { // json
	    ps.println("{  \"fragments\": [");
	    while (rset.next()) {
		if (rows++ > 0) {
		    ps.println(",");
		}
		ps.println("  {\"fragment-id\": "
			   +rset.getString("class_id")+",");
		ps.println("   \"smiles\": \""+rset.getString("smiles")+"\",");
		ps.println("   \"atoms\": "+rset.getString("acount")+",");
		ps.println("   \"bonds\": "+rset.getString("bcount")+",");
		ps.println("   \"symmetry\": "+rset.getString("symmetry")+",");
		ps.println("   \"complexity\": "+rset.getString("complexity")+",");
		ps.println("   \"snr\": "+String.format
			   ("%1.3f", rset.getDouble("snr"))+",");
		ps.println("   \"apt\": "+String.format
			   ("%1.3f", rset.getDouble("apt"))+",");
		ps.print("   \"count\": "+rset.getString("instances")+"}");
	    }
	    if (rows > 0) {
		ps.println();
	    }
	    ps.println(" ]");
	    ps.println("}");
	}
	rset.close();
	stm.close();
	pstm.close();
	con.close();
	ps.flush();
	logger.info(rows+" fragment(s) fetch in "
		    +String.format("%1$.3fs",1e-3*(System.currentTimeMillis()
						   -start)));
    }

    // base64 encoded
    String getZScores (PreparedStatement pstm, String classId) 
	throws SQLException, IOException {

	pstm.setString(1, classId);
	ResultSet rs = pstm.executeQuery();
	List<Float> zscores = new ArrayList<Float>();
	while (rs.next()) {
	    zscores.add(rs.getFloat("zscore"));
	}
	rs.close();

	ByteArrayOutputStream bos = new ByteArrayOutputStream ();
	byte[] buf = new byte[4];
	bos.write(encode (zscores.size(), buf), 0, buf.length);
	for (Float z : zscores) {
	    int iz = Float.floatToIntBits(z);
	    bos.write(encode (iz, buf), 0, buf.length);
	}
	return Base64.encodeToString(bos.toByteArray(), true);
    }

    static byte[] encode (int x, byte[] b) {
	b[0] = (byte)(x>>24);
	b[1] = (byte)(x>>16);
	b[2] = (byte)(x>>8);
	b[3] = (byte)(x&0xff);
	return b;
    }
}
